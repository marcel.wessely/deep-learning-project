import os
import pathlib
import pandas as pd
import numpy as np
import torch
import torchvision
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import cv2
from torch.utils.data import TensorDataset

PROJECT_PATH = pathlib.Path(
    __file__).parent.absolute().as_posix()

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')


def import_Ames():
    """ Reads the complete dataset and filters by disease, bundesland, landkreis"""
    train = pd.read_csv('{}/data/{}'.format(PROJECT_PATH, "ames/train.csv"), delimiter=",", dtype=object)
    test = pd.read_csv('{}/data/{}'.format(PROJECT_PATH, "ames/test.csv"), delimiter=",", dtype=object)
    label = pd.read_csv('{}/data/{}'.format(PROJECT_PATH, "ames/sample_submission.csv"), delimiter=",", dtype=float)[
        'SalePrice']
    test['SalePrice'] = label
    df = train.append(test, ignore_index=True)
    result = pd.DataFrame({})
    for key, value in df.iteritems():
        unique = df[key].unique()
        test_nominal = unique[0]
        if type(test_nominal) != str and np.isnan(test_nominal):
            test_nominal = unique[1]
        if type(test_nominal) != str or test_nominal.isnumeric():
            result[key] = value
        else:
            for name in unique:
                str_name = "NaN" if type(name) != str and np.isnan(name) else name
                if str_name == "NaN":
                    result[key + "_" + str_name] = pd.isna(value)
                else:
                    result[key + "_" + str_name] = value == name
                result[key + "_" + str_name].fillna(0)
    train = Ames_generate_dataset_for_dataloader(result[:1461])
    test = Ames_generate_dataset_for_dataloader(result[1461:])
    return train, test


def Ames_generate_dataset_for_dataloader(train_df):
    train_inputs, train_targets = Ames_select(train_df)
    train_inputs = torch.tensor(train_inputs, dtype=torch.float32)
    train_targets = torch.tensor(train_targets, dtype=torch.float32)
    # Create dataset out of trainings data
    train_data = torch.utils.data.TensorDataset(train_inputs, train_targets)
    return train_data


def Ames_select(df):
    X = df.drop('SalePrice', axis=1).drop('Id', axis=1).fillna(0).to_numpy()
    X = X.astype(float)
    Y = df['SalePrice'].to_numpy()
    Y = Y.astype(float)
    return X, Y


def import_MNIST():
    transform = transforms.Compose(
        [transforms.ToTensor(),
         transforms.Normalize((0.5,), (0.5,))])  # For now chosen randomly!
    trainset_MNIST = torchvision.datasets.MNIST(root="./data", train=True, download=True, transform=transform)
    testset_MNIST = torchvision.datasets.MNIST(root="./data", train=False, download=True, transform=transform)

    return trainset_MNIST, testset_MNIST


# I reduced the data set to 10 fruits: ['Apple (Red 1)', 'Banana', 'Cocos', 'Eggplant', 'Hazelnut', 'Kiwi',
# 'Maracuja', 'Onion (White)', 'Papaya', 'Strawberry']
def import_Fruit():
    imgs = []
    labels = []
    imgs_test = []
    labels_test = []
    root = 'data/fruits-360/Training'
    root_test = 'data/fruits-360/Test'
    fruits = os.listdir(root)
    for i, fruit in enumerate(fruits):
        for j, image_path in enumerate(os.listdir(root + '/' + fruit)):
            img = read_img(fruit, image_path, root)
            imgs.append(img)
            labels.append(i)
        for j, image_path in enumerate(os.listdir(root_test + '/' + fruit)):
            img = read_img(fruit, image_path, root_test)
            imgs_test.append(img)
            labels_test.append(i)
    train = fruit_generate_dataset_for_dataloader([imgs, labels])
    test = fruit_generate_dataset_for_dataloader([imgs_test, labels_test])
    return train, test


def read_img(fruit, image_path, root_test):
    img = cv2.imread(root_test + '/' + fruit + '/' + image_path)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    return np.swapaxes(img, 0, 2)


def fruit_generate_dataset_for_dataloader(train_df):
    train_inputs, train_targets = fruit_select(train_df)
    train_inputs = torch.tensor(train_inputs, dtype=torch.float32)
    train_targets = torch.tensor(train_targets, dtype=torch.long)
    # Create dataset out of trainings data
    train_data = torch.utils.data.TensorDataset(train_inputs, train_targets)
    return train_data


def fruit_select(df):
    X = np.array(df[0]).astype(float)
    Y = df[1]
    shape = (len(Y), 10)
    rows = np.arange(len(Y))
    one_hot = np.zeros(shape)
    one_hot[rows, Y] = 1
    return X, np.array(Y)


def import_SVHN():
    transform = transforms.Compose(
        [transforms.ToTensor(),
         transforms.Normalize((0.5,), (0.5,))])
    trainset = datasets.SVHN('datasets/SVHN/train/', split='train', download=True, transform=transform)
    testset = datasets.SVHN('datasets/SVHN/train/', split='test', download=True, transform=transform)

    trainset = torch.utils.data.Subset(trainset,np.arange(0,3000))
    testset = torch.utils.data.Subset(testset,np.arange(0,1000))

    return trainset, testset


def get_name(object):
    name = object.__class__.__name__
    if name == 'qAdam':
        name += "_q" + str(object.defaults['q'])
    if name == 'qAdagrad':
        name += "_q" + str(object.defaults['q'])
    return name
