import numpy as np


def calc_eval(real: np.ndarray, pred: np.ndarray, dataset: str):
    print(real, pred)
    err = real - pred
    mse = np.mean(np.square(err))
    rmse = np.sqrt(mse)
    nrmse = rmse / (real.max() - real.min())
    mae = np.mean((np.abs(err)))
    mdae = np.median(np.abs(err))
    smape = 100 * np.mean(np.abs(err) / ((np.abs(real) + np.abs(pred)) / 2))

    print()
    print('Accuracy for dataset {}:'.format(dataset))
    print('MAE: {}'.format(mae))
    print('MSE: {}'.format(mse))
    print('RMSE: {}'.format(rmse))
    print('NRMSE: {}'.format(nrmse))
    print('MDAE: {}'.format(mdae))
    print('SMAPE: {}'.format(smape))

def small_eval(real: np.ndarray, pred: np.ndarray, dataset: str):
    #print("real labels: \n", real)
    #print("predicted labels: \n", pred)

    err = real - pred
    mse = np.mean(np.square(err))
    rmse = np.sqrt(mse)
    accuracy = sum((real - pred) == 0) / len(real)

    print('Accuracy for {}:'.format(dataset))
    print('RMSE: {}'.format(rmse))
    print('Accuracy: {}'.format(accuracy))
    return rmse, accuracy
