import time

import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

from torch.optim.lr_scheduler import MultiStepLR
from torch.utils.data import DataLoader
from torch.utils.data import TensorDataset

import utils
from qAdam import qAdam
import matplotlib.pyplot as plt


def num_flat_features(x):
    # I don't know why we should need that but its in the tutorial so I leave it here
    size = x.size()[1:]  # all dimensions except the batch dimension
    num_features = 1
    for s in size:
        num_features *= s
    return num_features


class SVHNFullyConnectedNet(nn.Module):

    def __init__(self, input_size, output_size, hidden_size):
        super(SVHNFullyConnectedNet, self).__init__()
        self.h1 = nn.Linear(input_size, hidden_size)
        self.h2 = nn.Linear(hidden_size, hidden_size)
        self.h3 = nn.Linear(hidden_size, output_size)

    def forward(self, x):
        x = F.relu(self.h1(torch.flatten(x, start_dim=1)))
        x = F.relu(self.h2(x))
        x = self.h3(x)
        return x

    def fit(self, train_df):
        self.train()

        # Create dataset out of trainings data
        train_loader = torch.utils.data.DataLoader(train_df, batch_size=1, shuffle=True)

        # Define loss and optimizer
        criterion = nn.CrossEntropyLoss()
        optimizer = torch.optim.Adam(self.parameters(), lr=0.005)

        # Train loop
        n_epochs = 5  

        losses = []
        for i in range(n_epochs):
            epoch_loss = 0
            for t, data in enumerate(train_loader, 0):
                
                # Put seq and target on gpu if used
                inputs, labels = data
                inputs = inputs.to(utils.device)
                labels = labels.to(utils.device)

                # Reset gradient
                optimizer.zero_grad()

                # Predict, get Loss and Optimize
                outputs = self.forward(inputs)
                loss = criterion(outputs, labels)
                loss.backward()
                optimizer.step()

                epoch_loss += loss.item()

            print('Epoch:   {} \t|Loss: {}'.format(i, epoch_loss))
            losses.append(epoch_loss)
            if epoch_loss < 1e-7:
                break

        # plt.plot(losses, label='Loss')
        # plt.xlabel('Epoch')
        # plt.ylabel('CrossEntropyLoss')
        # plt.show()

    def predict(self, data):
        """Returns most likeliest class as number in [0, 9]"""
        self.eval()
        x = self.forward(data)
        return torch.argmax(x, dim=1)



class SVHN_CNN(nn.Module):
    def __init__(self, batch_size=32, n_epochs=80, learning_rate=0.000001, criterion=None, optimizer=None):
        super(SVHN_CNN, self).__init__()
        self.batch_size = batch_size
        self.n_epochs = n_epochs
        self.learning_rate = learning_rate

        # Input channels = 3, output channels = 18
        self.conv1 = torch.nn.Conv2d(3, 18, kernel_size=3, stride=1, padding=1)
        self.pool1 = torch.nn.MaxPool2d(kernel_size=2, stride=2, padding=0)
        self.conv2 = torch.nn.Conv2d(18, 18, kernel_size=3, stride=1, padding=1)
        self.pool2 = torch.nn.MaxPool2d(kernel_size=2, stride=2, padding=0)

        # 4608 input features, 64 output features (see sizing flow below)
        self.fc1 = torch.nn.Linear(18 * 8 * 8, 256)

        # 64 input features, 10 output features for our 10 defined classes
        self.fc2 = torch.nn.Linear(256, 10)

        # Define loss and optimizer
        if criterion is None:
            self.criterion = nn.CrossEntropyLoss()
        else:
            self.criterion = criterion
        if optimizer is None:
            self.optimizer = qAdam(self.parameters(), lr=learning_rate, q={1: 0.5, 2: 0.5}, amsgrad=True)
        else:
            self.optimizer = optimizer

    def forward(self, x):
        # Computes the activation of the first convolution
        # Size changes from (3, 32, 32) to (18, 32, 32)
        x = F.relu(self.conv1(x))

        # Size changes from (18, 32, 32) to (18, 16, 16)
        x = self.pool1(x)

        # Size changes from (18, 16, 16) to (18, 16, 16)
        x = F.relu(self.conv2(x))

        # Size changes from (18, 16, 16) to (18, 8, 8)
        x = self.pool1(x)

        # Reshape data to input to the input layer of the neural net
        # Size changes from (18, 8, 8) to (1, 1152)
        # Recall that the -1 infers this dimension from the other given dimension
        x = x.view(-1, 18 * 8 * 8)

        # Computes the activation of the first fully connected layer
        # Size changes from (1, 1152) to (1, 256)
        x = F.relu(self.fc1(x))

        # Computes the second fully connected layer (activation applied later)
        # Size changes from (1, 256) to (1, 10)
        x = self.fc2(x)
        return x

    def fit(self, train_data):
        # Print all of the hyperparameters of the training iteration:
        print("===== HYPERPARAMETERS =====")
        print("batch_size=", self.batch_size)
        print("epochs=", self.n_epochs)
        print("learning_rate=", self.learning_rate)
        print("=" * 30)

        self.train()
        train_loader = DataLoader(dataset=train_data, batch_size=self.batch_size, shuffle=True)

        # Define decaying learning rate
        # TODO: Is this meant ot be only for one step/milestone?
        scheduler = MultiStepLR(self.optimizer, milestones=[100], gamma=0.9)

        # Train loop
        losses = []
        for i in range(self.n_epochs):
            epoch_loss = 0

            for x, y in train_loader:
                # Put seq and target on gpu if used
                x = x.to(utils.device)
                y = y.to(utils.device)

                # Reset gradient
                self.optimizer.zero_grad()

                # Predict, get Loss and Optimize
                output = self.forward(x)
                loss = self.criterion(output, y)
                loss.backward()
                self.optimizer.step()
                epoch_loss += loss.item()
            print('Epoch:   {} \t|Loss: {}'.format(i, epoch_loss))
            losses.append(epoch_loss)
            scheduler.step()
            if epoch_loss < 0.5:
                break

        # plt.plot(losses, label='Loss')
        # plt.xlabel('Epoch')
        # plt.ylabel('MSE')
        # plt.show()

        return losses



    def predict(self, data):
        """Returns most likeliest class as number in [0, 9]"""
        self.eval()
        '''if len(img) == 3:
            x = torch.tensor([img], dtype=torch.float32)
            y = self.forward(x)
            return torch.argmax(y, dim=1)

        x = torch.tensor([img[0]], dtype=torch.float32)
        y = self.forward(x)
        all_pred = torch.argmax(y, dim=1)
        for image in img[1:]:
            x = torch.tensor([image], dtype=torch.float32)
            y = self.forward(x)
            all_pred = torch.cat((all_pred, torch.argmax(y, dim=1)), 0)
        return all_pred'''
        x = self.forward(data)
        return torch.argmax(x, dim=1)

def accuracy():
    correct = 0
    for x, y in zip(test[0], test[1]):
        prediction = model.predict(x).item()
        if prediction == y:
            correct += 1
    print(correct / len(test[1]))


if __name__ == "__main__": 

    # Load data and get training set
    train, test = utils.import_SVHN()   
    model = SVHN_CNN().to(utils.device)
    model.fit(train)
    #prediction = model.predict(test)

    #eval.calc_eval(prediction.to_numpy(), test['SalePrice'].to_numpy().astype(float), "AMES")
