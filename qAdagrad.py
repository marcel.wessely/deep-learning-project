import torch
from torch.optim.optimizer import Optimizer


class qAdagrad(Optimizer):
    """Implements Adagrad algorithm.
    It has been proposed in `Adaptive Subgradient Methods for Online Learning
    and Stochastic Optimization`_.
    Arguments:
        params (iterable): iterable of parameters to optimize or dicts defining
            parameter groups
        lr (float, optional): learning rate (default: 1e-2)
        lr_decay (float, optional): learning rate decay (default: 0)
        weight_decay (float, optional): weight decay (L2 penalty) (default: 0)
        eps (float, optional): term added to the denominator to improve
            numerical stability (default: 1e-10)
    .. _Adaptive Subgradient Methods for Online Learning and Stochastic
        Optimization: http://jmlr.org/papers/v12/duchi11a.html
            :param q Union[float, dict[float: float]].
            If only a float is passed the corresponding p-norm
            (called q in code to seperate from parameters p) is taken.
            Else the convex combination of the p-norms defined by the keys and
            lambdas of the values of the dict is taken
    """

    def __init__(self, params, lr=1e-2, lr_decay=0, weight_decay=0, initial_accumulator_value=0, eps=1e-10, q=2):
        if not 0.0 <= lr:
            raise ValueError("Invalid learning rate: {}".format(lr))
        if not 0.0 <= lr_decay:
            raise ValueError("Invalid lr_decay value: {}".format(lr_decay))
        if not 0.0 <= weight_decay:
            raise ValueError("Invalid weight_decay value: {}".format(weight_decay))
        if not 0.0 <= initial_accumulator_value:
            raise ValueError("Invalid initial_accumulator_value value: {}".format(initial_accumulator_value))
        if not 0.0 <= eps:
            raise ValueError("Invalid epsilon value: {}".format(eps))

        defaults = dict(lr=lr, lr_decay=lr_decay, eps=eps, weight_decay=weight_decay,
                        initial_accumulator_value=initial_accumulator_value, q=q)
        super(qAdagrad, self).__init__(params, defaults)
        qs = q
        if not isinstance(qs, dict):
            qs = {float(qs): 1}
        for group in self.param_groups:
            for p in group['params']:
                state = self.state[p]
                state['step'] = 0
                for e in qs:
                    state['sum_' + str(e)] = torch.full_like(p, initial_accumulator_value,
                                                             memory_format=torch.preserve_format)

    def share_memory(self):
        for group in self.param_groups:
            for p in group['params']:
                state = self.state[p]
                state['sum'].share_memory_()

    @torch.no_grad()
    def step(self, closure=None):
        """Performs a single optimization step.
        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            with torch.enable_grad():
                loss = closure()

        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue
                qs = group['q']
                if not isinstance(qs, dict):
                    qs = {float(qs): 1}

                grad = p.grad
                state = self.state[p]

                state['step'] += 1


                if group['weight_decay'] != 0:
                    if p.grad.is_sparse:
                        raise RuntimeError("weight_decay option is not compatible with sparse gradients")
                    grad = grad.add(p, alpha=group['weight_decay'])

                clr = group['lr'] / (1 + (state['step'] - 1) * group['lr_decay'])
                convex = None
                for q, w in qs.items():
                    state['sum_' + str(q)].add_(torch.abs(grad) ** q)
                    std = (state['sum_' + str(q)] ** (1 / q)).add_(group['eps'])
                    if convex is None:
                        convex = std * w
                    else:
                        convex.add_(std * w)
                p.addcdiv_(grad, convex, value=-clr)

        return loss
