from qAdagrad import qAdagrad
from SVHN import SVHNFullyConnectedNet, SVHN_CNN
from ames import AmesNet
from fruit import Fruit360CNN
from qAdam import qAdam
from utils import *
from networks import *
from torch.optim import Adam, Adagrad, SGD
from collections import defaultdict
import eval
import argparse
import os
import time
import re
import csv

base_lr = 0.005
base_lr_ada = base_lr * 10


def main():
    # for reproducibility
    torch.manual_seed(0)
    np.random.seed(0)

    RUNS = 3

    os.makedirs("plt", exist_ok=True)
    os.makedirs("results", exist_ok=True)

    print("Evaluation start. ")
    # Array with [NN, [train_set, test_set]]
    nn_and_dataset = [[MNISTFullyConnectedNet, import_MNIST()],
                      [Fruit360CNN, import_Fruit()],
                      [AmesNet, import_Ames()],
                      [SVHN_CNN, import_SVHN()]]

    # load data, network and more
    for init_net, [train_data, test_data] in nn_and_dataset:
        # sets the base_net which we use for initialization to device so that optimizer is also on the right device
        # base_net = init_net()
        # base_net.to(utils.device)

        optimizers = [[Adam, {'lr': base_lr}],
                      [qAdam, {'lr': base_lr, 'q': 2}],
                      [qAdam, {'lr': base_lr, 'q': {2: 0.5, 3: 0.5}}],
                      [qAdam, {'lr': base_lr, 'q': {1: 0.3, 2: 0.4, 3: 0.3}}],
                      [qAdam, {'lr': base_lr, 'q': {1: 0.5, 2: 0.5}}],
                      [qAdam, {'lr': base_lr, 'q': 1}],
                      [qAdam, {'lr': base_lr, 'q': 5}],
                      [Adagrad, {'lr': base_lr}],
                      [qAdagrad, {'lr': base_lr_ada, 'q': 2}],
                      [qAdagrad, {'lr': base_lr_ada, 'q': 1}],
                      [qAdagrad, {'lr': base_lr_ada, 'q': 5}],
                      ]

        # [qAdam, {'lr': base_lr, 'q': {1: 0.5, 10: 0.5}}],
        # [qAdam, {'lr': base_lr, 'q': 1.5}],
        # [qAdam, {'lr': base_lr, 'q': 2.5}],
        # [qAdagrad, {'lr': base_lr_ada, 'q': {1: 0.5, 3: 0.5}}],
        # [qAdagrad, {'lr': base_lr_ada, 'q': {1: 0.2, 2: 0.6, 3: 0.2}}],
        # [qAdagrad, {'lr': base_lr_ada, 'q': {1: 0.5, 2: 0.5}}],
        # [qAdagrad, {'lr': base_lr_ada, 'q': {1: 0.5, 10: 0.5}}],
        # [qAdagrad, {'lr': base_lr_ada, 'q': 2.5}],

        # qAdam(base_net.parameters(), lr=0.005, , amsgrad=True),
        # qAdam(base_net.parameters(), lr=0.005, q=0.5, amsgrad=True), # takes really long and is not good
        # qAdam(base_net.parameters(), lr=0.005, q=2, amsgrad=True),
        # qAdam(base_net.parameters(), lr=0.005, q=3, amsgrad=True),
        # qAdam(base_net.parameters(), lr=0.005, q=5, amsgrad=True),
        # qAdam(base_net.parameters(), lr=0.005, q=5, amsgrad=True),
        # qAdam(base_net.parameters(), lr=0.005, q=5, amsgrad=True),
        # qAdam(base_net.parameters(), lr=0.005, q=5, amsgrad=True),
        # qAdam(base_net.parameters(), lr=0.005, q=10, amsgrad=True), # That is shit
        # qAdam(base_net.parameters(), lr=0.005, q=20, amsgrad=True) # That is total shit
        # qAdam(base_net.parameters(), lr=0.005, q=1.5, amsgrad=True),
        # qAdam(base_net.parameters(), lr=0.005, q=2.5, amsgrad=True),

        dict_times = defaultdict(list)
        for optimizer in optimizers:
            # base_net.optimizer = optimizer
            base_net = init_net()

            all_losses = []
            print('=' * 40)
            for run in range(1, RUNS + 1):
                print("\nRunning: {}/{}".format(run, RUNS), get_name(base_net), get_name(optimizer[0](base_net.parameters())),
                      get_name(base_net.criterion))

                # setup network with optimizer
                # net = copy.deepcopy(base_net)
                # net.to(utils.device)
                base_net = init_net()
                base_net.to(utils.device)
                params = optimizer[1]
                if 'q' in params:
                    base_net.optimizer = optimizer[0](base_net.parameters(), lr=params['lr'], q=params['q'])
                else:
                    base_net.optimizer = optimizer[0](base_net.parameters(), lr=params['lr'])

                # train network
                start_time = time.time()
                losses = base_net.fit(train_data)
                end_time = time.time()
                all_losses.append(losses)

                # saves time into dict
                dict_times[get_name(base_net.optimizer)].append(end_time - start_time)

                # predict elements with network on whole test set
                test_loader = torch.utils.data.DataLoader(test_data, batch_size=len(test_data))
                dataiter = iter(test_loader)
                images, labels = dataiter.next()
                prediction = base_net.predict(images.to(utils.device))

                # evaluate error
                eval.small_eval(labels.cpu().detach().numpy(), prediction.cpu().detach().numpy(), get_name(base_net))
            np.savetxt("results/" + re.sub("[^0-9a-zA-Z]+", "_", get_name(base_net.optimizer)) + '-times.txt', dict_times[get_name(base_net.optimizer)], delimiter=',')
            
            name = "results/" + re.sub("[^0-9a-zA-Z]+", "_", get_name(base_net.optimizer)) + '-loss.txt'
            with open(name,"w") as f:
                wr = csv.writer(f)
                wr.writerows(all_losses)

            # plot loss
            print(all_losses)
            # loss_mean = np.mean(np.array(all_losses), axis=0)
            # TODO: loss_std

            fastestLoss = min(all_losses, key=len)
            plt.plot(np.array(fastestLoss), label=f'Loss of one {get_name(base_net.optimizer)}')

        plt.figure(figsize=(30, 10))
        plt.title(f"Loss of {get_name(base_net)}")
        plt.xlabel('Epoch')
        plt.ylabel(f'{get_name(base_net.criterion)}')
        plt.legend(loc='upper right')
        plt.savefig(f"plt/loss_{get_name(base_net)}", bbox_inches="tight")
        plt.show()

        # plots bar chart
        names = [x + key for key in dict_times.keys() for x in ["min_", "max_", "mean_"]]
        values = ([[min(x), max(x), sum(x) / len(x)] for x in dict_times.values()])
        values = np.array(values)
        time_min = values[:, 0]
        time_max = values[:, 1]
        time_mean = values[:, 2]
        # values = [item for sublist in values for item in sublist]

        x_pos = np.arange(len(dict_times.keys()))
        plt.title(f"Training times of {get_name(base_net)}")
        plt.ylabel("Time in seconds")
        plt.errorbar(x_pos, time_mean, [time_mean - time_min, time_max - time_mean], fmt='o')

        plt.ylim(ymin=0, ymax=np.median(time_mean) * 2)
        plt.xticks(x_pos, dict_times.keys(), rotation=90)
        plt.savefig(f"plt/time_{get_name(base_net)}", bbox_inches="tight")
        plt.show()

    print("Evaluation done. ")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Runs the evaluation. ")
    parser.add_argument("--no_cuda", help="Uses CPU instead of GPU for calucations. ", action="store_true")

    args = parser.parse_args()
    if args.no_cuda:
        utils.device = torch.device('cpu')  # overwrites the parameter in utils.device
        print("Uses CPU for calucations. ")

    print("Using", utils.device)
    main()
