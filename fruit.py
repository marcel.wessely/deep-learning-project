import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.optim.lr_scheduler import MultiStepLR
from torch.utils.data import DataLoader
from torch.utils.data import TensorDataset

import utils
from qAdagrad import qAdagrad
from qAdam import qAdam

device = utils.device
import matplotlib.pyplot as plt


class Fruit360CNN(nn.Module):
    def __init__(self, batch_size=32, n_epochs=100, learning_rate=0.000001, criterion=None, optimizer=None):
        super(Fruit360CNN, self).__init__()
        self.batch_size = batch_size
        self.n_epochs = n_epochs
        self.learning_rate = learning_rate

        # Input channels = 3, output channels = 18
        self.conv1 = torch.nn.Conv2d(3, 18, kernel_size=3, stride=1, padding=1)
        self.pool1 = torch.nn.MaxPool2d(kernel_size=2, stride=2, padding=0)
        self.conv2 = torch.nn.Conv2d(18, 18, kernel_size=3, stride=1, padding=1)
        self.pool2 = torch.nn.MaxPool2d(kernel_size=2, stride=2, padding=0)

        # 4608 input features, 64 output features (see sizing flow below)
        self.fc1 = torch.nn.Linear(18 * 25 * 25, 256)

        # 64 input features, 10 output features for our 10 defined classes
        self.fc2 = torch.nn.Linear(256, 10)

        # Define loss and optimizer
        if criterion is None:
            self.criterion = nn.CrossEntropyLoss()
        else:
            self.criterion = criterion
        if optimizer is None:
            self.optimizer = qAdam(self.parameters(), lr=learning_rate, q=1.5, amsgrad=True)
        else:
            self.optimizer = optimizer

    def forward(self, x):
        # Computes the activation of the first convolution
        # Size changes from (3, 100, 100) to (18, 100, 100)
        x = F.relu(self.conv1(x))

        # Size changes from (18, 100, 100) to (18, 50, 50)
        x = self.pool1(x)

        # Size changes from (18, 50, 50) to (18, 50, 50)
        x = F.relu(self.conv2(x))

        # Size changes from (18, 50, 50) to (18, 25, 25)
        x = self.pool1(x)

        # Reshape data to input to the input layer of the neural net
        # Size changes from (18, 25, 25) to (1, 11250)
        # Recall that the -1 infers this dimension from the other given dimension
        x = x.view(-1, 18 * 25 * 25)

        # Computes the activation of the first fully connected layer
        # Size changes from (1, 11250) to (1, 256)
        x = F.relu(self.fc1(x))

        # Computes the second fully connected layer (activation applied later)
        # Size changes from (1, 256) to (1, 10)
        x = self.fc2(x)
        return x

    def fit(self, train_df):
        self.train()
        train_loader = DataLoader(dataset=train_df, batch_size=self.batch_size, shuffle=True)

        # Train loop
        losses = []
        for i in range(self.n_epochs):
            epoch_loss = 0
            for x, y in train_loader:
                # Put seq and target on gpu if used
                x = x.to(utils.device)
                y = y.to(utils.device)

                # Reset gradient
                self.optimizer.zero_grad()

                # Predict, get Loss and Optimize
                output = self.forward(x)
                loss = self.criterion(output, y)
                loss.backward()
                self.optimizer.step()
                epoch_loss += loss.item()
            print('Epoch:   {} \t|Loss: {}'.format(i, epoch_loss))
            losses.append(epoch_loss)
            if epoch_loss < 7.5:
                break

        plt.plot(losses, label='Loss')
        plt.xlabel('Epoch')
        plt.ylabel('MSE')
        plt.show()

        return losses


    def predict(self, data):
        """Returns most likeliest class as number in [0, 9]"""
        self.eval()
        '''if len(img) == 3:
            x = torch.tensor([img], dtype=torch.float32)
            y = self.forward(x)
            return torch.argmax(y, dim=1)

        x = torch.tensor([img[0]], dtype=torch.float32)
        y = self.forward(x)
        all_pred = torch.argmax(y, dim=1)
        for image in img[1:]:
            x = torch.tensor([image], dtype=torch.float32)
            y = self.forward(x)
            all_pred = torch.cat((all_pred, torch.argmax(y, dim=1)), 0)
        return all_pred'''
        x = self.forward(data)
        return torch.argmax(x, dim=1)


if __name__ == "__main__":
    # Load data and get training set
    train, test = utils.import_Fruit()

    # Create, train and use model to predict new values
    model = Fruit360CNN().to(utils.device)
    model.fit(train)
    # accuracy() #TODO: Not working right now as we switched to new data import for test_set

