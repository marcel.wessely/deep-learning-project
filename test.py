import numpy as np
import matplotlib.pyplot as plt
import torch
import torchvision.datasets
import torchvision.transforms as transforms
# https://pytorch.org/get-started/locally/
import utils
from networks import MNISTFullyConnectedNet
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F


if __name__ == '__main__':
    utils.device = torch.device('cpu')

    # for reproduciability
    torch.manual_seed(0)
    np.random.seed(0)

    # https://pytorch.org/tutorials/beginner/blitz/cifar10_tutorial.html#sphx-glr-beginner-blitz-cifar10-tutorial-py
    transform = transforms.Compose(
        [transforms.ToTensor(),
         transforms.Normalize((0.5,), (0.5,))])

    trainset_MNIST = torchvision.datasets.MNIST(root="./data", train=True, download=True, transform=transform)
    testset_MNIST = torchvision.datasets.MNIST(root="./data", train=True, download=True, transform=transform)

    train_loader = torch.utils.data.DataLoader(trainset_MNIST, batch_size=50, shuffle=True)
    testloader = torch.utils.data.DataLoader(testset_MNIST, batch_size=4, shuffle=False)

    '''
    def imshow(img):
        img = img / 2 + 0.5  # unnormalize
        npimg = img.numpy()
        plt.imshow(np.transpose(npimg, (1, 2, 0)))
        plt.show()
        
    # show images
    imshow(torchvision.utils.make_grid(images))
    input_size = len(images[0, 0]) ** 2 # == 784
    '''

    MNISTNet = MNISTFullyConnectedNet(784, 10, 100)
    # print(MNISTNet)
    # get some random training images
    dataiter = iter(train_loader)
    # images, labels = dataiter.next()

    for t in range(1):
        images, labels = dataiter.next()
        prediction = MNISTNet.predict(images)
        print("prediction: ", prediction, "\nactual label: ", labels, "\n")

    MNISTNet.fit(trainset_MNIST)

    for t in range(1):
        images, labels = dataiter.next()
        prediction = MNISTNet.predict(images)
        print("prediction: ", prediction, "\nactual label: ", labels, "\n")
