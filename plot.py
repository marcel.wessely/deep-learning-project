from qAdagrad import qAdagrad
from SVHN import SVHNFullyConnectedNet, SVHN_CNN
from ames import AmesNet
from fruit import Fruit360CNN
from qAdam import qAdam
from utils import *
from networks import *
from torch.optim import Adam, Adagrad, SGD
from collections import defaultdict
import eval
import argparse
import os
import time
import re
import csv

base_lr = 0.005
base_lr_ada = base_lr * 10

def plot():
    
    # plots bar chart
    optimizers = [[Adam, {'lr': base_lr}],
                  [qAdam, {'lr': base_lr, 'q': 2}],
                  [qAdam, {'lr': base_lr, 'q': {2: 0.5, 3: 0.5}}],
                  [qAdam, {'lr': base_lr, 'q': {1: 0.3, 2: 0.4, 3: 0.3}}],
                  [qAdam, {'lr': base_lr, 'q': {1: 0.5, 2: 0.5}}],
                  [qAdam, {'lr': base_lr, 'q': 1}],
                  [qAdam, {'lr': base_lr, 'q': 5}],
                  [Adagrad, {'lr': base_lr_ada}],
                  [qAdagrad, {'lr': base_lr_ada, 'q': 2}],
                  [qAdagrad, {'lr': base_lr_ada, 'q': 1}],
                  [qAdagrad, {'lr': base_lr_ada, 'q': 5}],
                  ]
    names = []
    times = []
    for opti in optimizers:
        params = opti[1]
        base_net = MNISTFullyConnectedNet()
        if 'q' in params:
            optimizer = opti[0](base_net.parameters(), lr=params['lr'], q=params['q'])
        else:
            optimizer = opti[0](base_net.parameters(), lr=params['lr'])
        x = np.loadtxt("results/MNIST/" + re.sub("[^0-9a-zA-Z]+", "_", get_name(optimizer)) + '-times.txt', delimiter=',')
        times.append(x)
        names.append(get_name(optimizer))

        with open("results/MNIST/" + re.sub("[^0-9a-zA-Z]+", "_", get_name(optimizer)) + '-loss.txt',"r") as f:
            wr = csv.reader(f)
            all_losses = list(wr)
            all_losses = [i for i in all_losses if i]
            
        
        fastestLoss = min(all_losses, key=len)
        fastestLoss = [float(x) for x in fastestLoss]
        plt.plot(np.array(fastestLoss), label=f'Loss of one {get_name(optimizer)}')

    plt.title(f"Loss of {get_name(base_net)}")
    plt.xlabel('Epoch')
    plt.ylabel(f'{get_name(base_net.criterion)}')
    plt.legend(loc='upper right')
    plt.savefig(f"plt/loss_{get_name(base_net)}", bbox_inches="tight")
    plt.show()

    values = ([[min(x), max(x), sum(x) / len(x)] for x in times])
    values = np.array(values)
    time_min = values[:, 0]
    time_max = values[:, 1]
    time_mean = values[:, 2]
    # values = [item for sublist in values for item in sublist]

    x_pos = np.arange(len(names))
    plt.figure()
    plt.title(f"Training times of {get_name(base_net)}")
    plt.ylabel("Time in seconds")
    plt.errorbar(x_pos, time_mean, [time_mean - time_min, time_max - time_mean], fmt='o')

    plt.ylim(ymin=0, ymax=np.median(time_mean) * 2)
    plt.xticks(x_pos, names, rotation=90)
    plt.savefig(f"plt/time_{get_name(base_net)}")
    plt.show()

plot()