# Deep Learning Project

Thomas, Niklas, Nico, Marcel 

In this project we analysed the normalization technique included in Adam and Adagrad. We generalized them to q-Adam (q-Adagrad) which instead of always using 2-norm (for weighted average of velocity for example), we can now support any convex-combination of any (including fractional) p-norms. 

In short different orders of norms could provide some computational advantage, though further computational studies are required to find the system behind the improvements.