import torch
import torch.nn as nn
import torch.nn.functional as F
import matplotlib.pyplot as plt

import utils


class MNISTFullyConnectedNet(nn.Module):

    def __init__(self, input_size=784, output_size=10, hidden_size=100, batch_size=50, n_epochs=50, learning_rate=0.005, criterion=None, optimizer=None):
        super(MNISTFullyConnectedNet, self).__init__()
        self.h1 = nn.Linear(input_size, hidden_size)
        self.h2 = nn.Linear(hidden_size, hidden_size)
        self.h3 = nn.Linear(hidden_size, output_size)

        self.batch_size = batch_size
        self.n_epochs = n_epochs
        self.learning_rate = learning_rate

        # Define loss and optimizer
        if criterion is None:
            self.criterion = nn.CrossEntropyLoss()
        else:
            self.criterion = criterion
        if optimizer is None:
            self.optimizer = torch.optim.Adam(self.parameters(), lr=self.learning_rate)
        else:
            self.optimizer = optimizer

    def forward(self, x):
        x = F.relu(self.h1(torch.flatten(x, start_dim=1)))
        x = F.relu(self.h2(x))
        x = self.h3(x)
        return x

    def fit(self, train_df):
        self.train()

        # Create dataset out of trainings data
        train_loader = torch.utils.data.DataLoader(train_df, batch_size=self.batch_size, shuffle=True)

        # Train loop
        losses = []
        for i in range(self.n_epochs):
            epoch_loss = 0
            for x, y in train_loader:
                # Put seq and target on gpu if used
                x = x.to(utils.device)
                y = y.to(utils.device)

                # Reset gradient
                self.optimizer.zero_grad()

                # Predict, get Loss and Optimize
                output = self.forward(x)
                loss = self.criterion(output, y)
                loss.backward()
                self.optimizer.step()
                epoch_loss += loss.item()
            print('Epoch:   {} \t|Loss: {}'.format(i, epoch_loss))
            losses.append(epoch_loss)
            if epoch_loss < 48:
                break

        return losses


    def predict(self, data):
        """Returns most likeliest class as number in [0, 9]"""
        self.eval()
        x = self.forward(data)
        return torch.argmax(x, dim=1)


