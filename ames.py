from torch.optim.adagrad import Adagrad

import utils
import torch
import torch.nn as nn
from torch.utils.data import TensorDataset
from torch.utils.data import DataLoader
import torch.nn.functional as F
import eval
from torch.optim.lr_scheduler import StepLR, ReduceLROnPlateau, MultiStepLR
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from qAdagrad import qAdagrad
from qAdam import qAdam

def RMSELoss(yhat,y):
    return torch.sqrt(torch.mean((yhat-y)**2) + 1e-6)

class AmesNet(nn.Module):
    def __init__(self, hidden=500, batch_size=20, n_epochs=200, learning_rate=0.05, criterion=None, optimizer=None):
        super(AmesNet, self).__init__()
        self.fc1 = nn.Linear(311, hidden)
        self.fc2 = nn.Linear(hidden, hidden)
        self.fc3 = nn.Linear(hidden, 1)

        self.batch_size = batch_size
        self.n_epochs = n_epochs
        self.learning_rate = learning_rate

        # Define loss and optimizer
        if criterion is None:
            self.criterion = RMSELoss
        else:
            self.criterion = criterion
        if optimizer is None:
            self.optimizer = qAdagrad(self.parameters(), lr=self.learning_rate, q={3: 0.5, 2: 0.5})
        else:
            self.optimizer = optimizer


    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

    def select(self, df):
        X = df.drop('SalePrice', axis=1).drop('Id', axis=1).fillna(0).to_numpy()
        X = X.astype(float)
        Y = df['SalePrice'].to_numpy()
        Y = Y.astype(float)
        return X, Y

    def generate_dataset_for_dataloader(self, train_df):
        train_inputs, train_targets = self.select(train_df)
        train_inputs = torch.tensor(train_inputs, dtype=torch.float32)
        train_targets = torch.tensor(train_targets, dtype=torch.float32)
        # Create dataset out of trainings data
        train_data = torch.utils.data.TensorDataset(train_inputs, train_targets)
        return train_data

    def fit(self, train_data, **kwargs):
        self.train()
        # train_data = self.generate_dataset_for_dataloader(train_df)
        train_loader = DataLoader(dataset=train_data, batch_size=self.batch_size)

        scheduler = MultiStepLR(self.optimizer, milestones=[100], gamma=0.9)

        # Train loop
        n_epochs = self.n_epochs

        losses = []
        for i in range(n_epochs):
            epoch_loss = 0
            for x, y in train_loader:
                # Put seq and target on gpu if used
                x = x.to(utils.device)
                y = y.to(utils.device)

                # Reset gradient
                self.optimizer.zero_grad()

                # Predict, get Loss and Optimize
                output = torch.reshape(self.forward(x), (-1,))
                loss = self.criterion(output, y)
                loss.backward()
                self.optimizer.step()
                epoch_loss += loss.item()
            print('Epoch:   {} \t|Loss: {}'.format(i, epoch_loss))
            losses.append(epoch_loss)
            scheduler.step()
            if epoch_loss < 1400000.0:
                break
        return losses

    def predict(self, data: pd.DataFrame):
        self.eval()
        return self.forward(data)



if __name__ == "__main__":
    # Load data and get training set
    train, test = utils.import_Ames()

    # Create, train and use model to predict new values
    model = AmesNet().to(utils.device)
    model.fit(train)

