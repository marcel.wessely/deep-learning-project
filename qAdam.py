import torch
import math
from torch.optim.optimizer import Optimizer
import numpy as np


class qAdam(Optimizer):
    r"""Implements Adam algorithm.

    It has been proposed in `Adam: A Method for Stochastic Optimization`_.

    Arguments:
        params (iterable): iterable of parameters to optimize or dicts defining
            parameter groups
        lr (float, optional): learning rate (default: 1e-3)
        betas (Tuple[float, float], optional): coefficients used for computing
            running averages of gradient and its square (default: (0.9, 0.999))
        eps (float, optional): term added to the denominator to improve
            numerical stability (default: 1e-8)
        weight_decay (float, optional): weight decay (L2 penalty) (default: 0)
        amsgrad (boolean, optional): whether to use the AMSGrad variant of this
            algorithm from the paper `On the Convergence of Adam and Beyond`_
            (default: False)
        :param q Union[float, dict[float: float]].
            If only a float is passed the corresponding p-norm
            (called q in code to seperate from parameters p) is taken.
            Else the convex combination of the p-norms defined by the keys and
            lambdas of the values of the dict is taken

    .. _Adam\: A Method for Stochastic Optimization:
        https://arxiv.org/abs/1412.6980
    .. _On the Convergence of Adam and Beyond:
        https://openreview.net/forum?id=ryQu7f-RZ
    .. _DeepLearningProject: The influence of p-norms for Stochastic Optimization
    """

    def __init__(self, params, lr=1e-3, betas=(0.9, 0.999), eps=1e-8,
                 weight_decay=0, amsgrad=True, q=2):
        if not 0.0 <= lr:
            raise ValueError("Invalid learning rate: {}".format(lr))
        if not 0.0 <= eps:
            raise ValueError("Invalid epsilon value: {}".format(eps))
        if not 0.0 <= betas[0] < 1.0:
            raise ValueError("Invalid beta parameter at index 0: {}".format(betas[0]))
        if not 0.0 <= betas[1] < 1.0:
            raise ValueError("Invalid beta parameter at index 1: {}".format(betas[1]))
        if not 0.0 <= weight_decay:
            raise ValueError("Invalid weight_decay value: {}".format(weight_decay))
        defaults = dict(lr=lr, betas=betas, eps=eps,
                        weight_decay=weight_decay, amsgrad=amsgrad, q=q)
        super(qAdam, self).__init__(params, defaults)

    def __setstate__(self, state):
        super(qAdam, self).__setstate__(state)
        for group in self.param_groups:
            group.setdefault('amsgrad', False)

    @torch.no_grad()
    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            with torch.enable_grad():
                loss = closure()

        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue
                grad = p.grad
                if grad.is_sparse:
                    raise RuntimeError('Adam does not support sparse gradients, please consider SparseAdam instead')
                amsgrad = group['amsgrad']

                state = self.state[p]
                qs = group['q']
                if not isinstance(qs, dict):
                    qs = {float(qs): 1}
                # State initialization
                if len(state) == 0:
                    state['step'] = 0
                    # Exponential moving average of gradient values
                    state['exp_avg'] = torch.zeros_like(p, memory_format=torch.preserve_format)
                    # Exponential moving average of squared gradient values
                    for q in qs:
                        state['exp_avg_sq_' + str(q)] = torch.zeros_like(p, memory_format=torch.preserve_format)
                        if amsgrad:
                            # Maintains max of all exp. moving avg. of sq. grad. values
                            state['max_exp_avg_sq_' + str(q)] = torch.zeros_like(p, memory_format=torch.preserve_format)

                exp_avg = state['exp_avg']
                exp_avg_q = [0] * len(qs)
                max_exp_avg_q = [0] * len(qs)
                for i, q in enumerate(qs):
                    exp_avg_q[i] = state['exp_avg_sq_' + str(q)]
                    if amsgrad:
                        max_exp_avg_q[i] = state['max_exp_avg_sq_' + str(q)]
                beta1, beta2 = group['betas']

                state['step'] += 1
                bias_correction1 = 1 - beta1 ** state['step']
                bias_correction2 = 1 - beta2 ** state['step']

                if group['weight_decay'] != 0:
                    grad = grad.add(p, alpha=group['weight_decay'])
                # Decay the first and second moment running average coefficient
                exp_avg.mul_(beta1).add_(grad, alpha=1 - beta1)
                for i, q in enumerate(qs):
                    exp_avg_q[i].mul_(beta2).add_(torch.abs(grad) ** q, alpha=1 - beta2)
                if amsgrad:
                    convex = None
                    for i, (q, w) in enumerate(qs.items()):
                        torch.max(max_exp_avg_q[i], exp_avg_q[i], out=max_exp_avg_q[i])
                        if convex is None:
                            convex = (max_exp_avg_q[i] ** (1 / q) / bias_correction2 ** (1 / q)) * w
                        else:
                            convex.add_((max_exp_avg_q[i] ** (1 / q) / bias_correction2 ** (1 / q)) * w)
                    denom = convex.add_(group['eps'])
                else:
                    convex = None
                    for i, (q, w) in enumerate(qs.items()):
                        if convex is None:
                            convex = (exp_avg_q[i] ** (1/q) / bias_correction2 ** (1/q)) * w
                        else:
                            convex.add_((exp_avg_q[i] ** (1/q) / bias_correction2 ** (1/q)) * w)
                    denom = convex.add_(group['eps'])

                step_size = group['lr'] / bias_correction1

                p.addcdiv_(exp_avg, denom, value=-step_size)

        return loss